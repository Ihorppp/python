from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import sys
from time import sleep


class ProductPage(object):

	def __init__(self, driver):

		if driver == "Firefox":
			self.driver = webdriver.Firefox()
			# self.driver = webdriver.Firefox(executable_path="C:\Users\computer\Downloads\geckodriver-v0.19.1-win64\geckodriver.exe")

		"""
		### Chrome webdriver and Gecko webdriver should be manually installed ###
		
		elif driver == "Chrome":							
			self.driver = webdriver.Chrome()
		elif driver == "PhantomJS":
			desired_capabilities = DesiredCapabilities.PHANTOMJS.copy()
			desired_capabilities['phantomjs.page.customHeaders.User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) ' \
                                                                  'AppleWebKit/537.36 (KHTML, like Gecko) ' \s
                                                                  'Chrome/39.0.2171.95 Safari/537.36'
			self.driver = webdriver.PhantomJS(desired_capabilities=desired_capabilities)
		"""

	def search(self, url, product_id, wait_time):
		self.driver.get(url)
		while True:
			#find table
			product_table = self.driver.find_element(*ProductPageLocators.PRODUCT_TABLE)
			#find all items in <li> tag
			items = product_table.find_elements(*ProductPageLocators.TABLE_ITEM)
			#loop through the items to get one with the specified product id
			for item in items:
				if item.get_attribute("data-asin")==product_id:
					product_link = item.find_element(*ProductPageLocators.PRODUCT_LINK)
					product_link.click()
					#wait for the specified time
					sleep(wait_time)
					return
			#element not found -> opening next page
			try:
				next_page = WebDriverWait(self.driver, 10).until(
	        		EC.visibility_of_element_located(ProductPageLocators.NEXT_PAGE))
				self.driver.get(next_page.get_attribute("href"))
			except Exception as e:
				print e
				break
			
class ProductPageLocators(object):
	PRODUCT_TABLE = (By.ID, "atfResults")
	TABLE_ITEM = (By.TAG_NAME, "li")
	PRODUCT_LINK = (By.XPATH, ".//a[@class='a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal']")
	NEXT_PAGE = (By.XPATH, "//a[@id='pagnNextLink']")


def main():
	
	p = ProductPage(sys.argv[1])#browser
	for i in range(int(sys.argv[5])):#number of repetitions
		p.search(sys.argv[2], sys.argv[3], int(sys.argv[4]))#url, product_id, wait_time
	p.driver.quit()

if __name__ == "__main__":
    main()

### USAGE EXAMPLE ###

### Format: browser choice(Firefox, Chrome, PhantomJS), link to page in quotes, product id, time in between repetitions, number of repetitions ###
### python auto_clicker.py Firefox "https://www.amazon.de/s/ref=sr_pg_1/259-0671315-2194545?rh=i%3Aaps%2Ck%3Akopfh%C3%B6rer+nylon&keywords=kopfh%C3%B6rer+nylon&ie=UTF8&qid=1518008110" B00R17TQGY 5 5 ###
### Please, note that provided link has to be in quotes ###
