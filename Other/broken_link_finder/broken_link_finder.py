from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import DesiredCapabilities
import requests
from collections import defaultdict

desired_capabilities = DesiredCapabilities.PHANTOMJS.copy()
desired_capabilities['phantomjs.page.customHeaders.User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) ' \
                                                                  'AppleWebKit/537.36 (KHTML, like Gecko) ' \
                                                                  'Chrome/39.0.2171.95 Safari/537.36'
driver = webdriver.PhantomJS(desired_capabilities=desired_capabilities)

URL = "https://somesite.com/"

#returns all links cantained on specified page
def get_links_from_page(url):
	driver.get(url)
	driver.implicitly_wait(5)
	temp = driver.find_elements_by_xpath("//a[@href]")
	links = [i.get_attribute("href") for i in temp]
	return links

#list containing all links from site
all_links = []

#all links with status_code not 200
broken_links = []

#holds {link_to_page: [broken_links_on_page]} pair
container = defaultdict(list)


def find_broken(_link):

	for link in get_links_from_page(_link):
		if "http" not in link: continue				#to avoid links like javascript:void(0)
		if "somesite.com" not in link: continue	#to avoid infinite recursion when leaving specified site
		if "#portal-cookie-info" in link: continue	#to drop useless links
		if "#" in link: continue					#to drop duplicate links containing # at the end
		#to avoid visiting the same links more then once
		if link not in all_links:
			all_links.append(link)

			if requests.get(link).status_code!=200:
				broken_links.append(link)
				container[_link].append(link)
			else:
				find_broken(link)

find_broken(URL)