from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from time import sleep
import logging


class ProfilePage(object):
	"""docstring for ProfilePage"""
	def __init__(self, driver):
		self.driver = driver
		logging.basicConfig(filename='./example.log', level=logging.DEBUG)


	def get_photo_link(self, url):
		self.driver.get(url)
		#class photo container
		try:
			node = self.driver.find_element(*ProfilePageLocators.PHOTO_CONTAINER)
		except NoSuchElementException as e:
			logging.error("Could not find photo container")
			return ""

		#link to profile photo in full format
		try:
			photo_link = node.find_element(*ProfilePageLocators.PHOTO_LINK).get_attribute("href")
			#silhouette
			if "/profile/picture/view/?" in photo_link:
				return "https://scontent-frt3-2.xx.fbcdn.net/v/t1.0-1/c47.0.160.160/p160x160/10354686_10150004552801856_220367501106153455_n.jpg?oh=3496c7c2b1069b9ff46c891612656ad7&oe=5B466A49"
		except NoSuchElementException as e:
			logging.error("Could not find profile main photo link")
			return ""

		self.driver.get(photo_link)

		sleep(3)
		#stage class
		try:
			node = self.driver.find_element(*ProfilePageLocators.STAGE)
		except NoSuchElementException as e:
			logging.error("Could not find photo stage")
			return ""

		#link to source
		try:
			download_link = node.find_element(*ProfilePageLocators.DOWNLOAD_LINK).get_attribute("src")
		except NoSuchElementException as e:
			try:
				download_link = node.find_element(*ProfilePageLocators.DOWNLOAD_LINK2).get_attribute("src")
			except NoSuchElementException as e:
				logging.error("Could not find link to profile photo source")
				return ""

		return download_link

class ProfilePageLocators(object):
	
	PHOTO_CONTAINER = (By.CLASS_NAME, "photoContainer")
	PHOTO_LINK = (By.XPATH, ".//a")
	STAGE = (By.CLASS_NAME, "stage")
	DOWNLOAD_LINK = (By.XPATH, ".//img")
	DOWNLOAD_LINK2 = (By.CLASS_NAME, "spotlight")