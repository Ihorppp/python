from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from time import sleep
import logging


selenium_logger = logging.getLogger('selenium')
selenium_logger.setLevel(logging.ERROR)

class BasePage(object):
	"""docstring for BasePage"""
	def __init__(self):

		self.driver = webdriver.Firefox()
		self.driver.implicitly_wait(10)

class LoginPage(BasePage):
	"""docstring for LoginPage"""
	def __init__(self, url):
		super(LoginPage, self).__init__()
		self.driver.get(url)
		logging.basicConfig(filename='./example.log', level=logging.DEBUG)

	def check_title(self, title):
		try:
			WebDriverWait(self.driver, 10).until(
				EC.title_contains(title))
		except TimeoutException as e:
			logging.error("Unable to load {0} page".format(title))
			
	def login(self, email, password):
		try:
			self.driver.find_element(*LoginPageLocator.EMAIL).send_keys(email)
		except NoSuchElementException as e:
			logging.error("Unable to find email field")
			
		try:
			self.driver.find_element(*LoginPageLocator.PASSWORD).send_keys(password)
		except NoSuchElementException as e:
			logging.error("Unable to find password field")
			
		#waiting 2s to not login instantly 
		sleep(2)
		try:
			self.driver.find_element(*LoginPageLocator.LOGIN_BUTTON).click()
		except NoSuchElementException as e:
			logging.error("Unable to find login button")
		
		sleep(3)		

class LoginPageLocator(object):
	"""docstring for ClassName"""
	EMAIL = (By.ID, "email")
	PASSWORD = (By.ID, "pass")
	LOGIN_BUTTON = (By.XPATH, "//input[@value='Log In']")