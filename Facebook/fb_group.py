from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from time import sleep
try:
	from sqlalchemy import create_engine
	import pandas as pd
except ImportError as e:
	print ("A required python module is missing!\n%s") % (e)
	sys.exit()
import logging


selenium_logger = logging.getLogger('selenium')
selenium_logger.setLevel(logging.ERROR)


class GroupPage(object):
	"""docstring for GroupPage"""
	def __init__(self, driver, id):
		self.driver = driver
		url = "https://www.facebook.com/groups/{0}/members/".format(id)
		self.driver.get(url)
		logging.basicConfig(filename='./example.log', level=logging.DEBUG)


	def scroll_to_bottom(self):
		logging.info("Scrolling started")
		webdriver.ActionChains(self.driver).send_keys(Keys.ESCAPE).perform()

		self.driver.implicitly_wait(0)
		while True:
			self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			try:
				self.driver.find_element(*GroupPageLocators.END)
				logging.info("Scrolling finished")
				break
			except NoSuchElementException as e:
				pass

	def get_all_members(self):
		logging.info("Extracting members")
		try:
			members = self.driver.find_elements(*GroupPageLocators.MEMBER)
			for member in members:
				yield member
			# return members
		except NoSuchElementException as e:
			logging.error("Could not fetch members")


	def get_data(self, db_host, db_username, db_pass, db_name, db_table):

		self.scroll_to_bottom()

		members = self.get_all_members()

		node = None; link = None; profile_link = None; name_lastname = None; work = None;

		engine = create_engine("mysql://"+db_username+":"+db_pass+"@"+db_host+"/"+db_name+"")
		con = engine.connect()

		count = 0

		for member in members:

			user_id = member.get_attribute("id").split("_")[-1]

			#skipping in case we could not get user id
			if not user_id.isdigit():
				continue

			try:
				node = member.find_element(*GroupPageLocators.NAME_LASTNAME)
			except NoSuchElementException as e:
				logging.error("Could not find node")

			try:
				link = node.find_element_by_xpath(".//a[@href]")
			except NoSuchElementException as e:
				logging.error("Could not find profile link")

			profile_url = link.get_attribute("href").split("?fref", 1)[0]
			profile_url = profile_url.split("&fref", 1)[0]

			name_lastname = link.text.encode("utf-8")
			first_name = name_lastname.split(' ', 1)[0]

			#ridiculus exception handling
			try:
				last_name = name_lastname.split(' ', 1)[1]
			except IndexError as e:
				last_name = ""

			try:
				work = node.find_element(*GroupPageLocators.WORK).text.encode("utf-8") 
			except NoSuchElementException as e:
				logging.error("Could not find work information")

			if " at " in work:
				position = work.split("at", 1)[0]
				try:
					company = work.split("at", 1)[1]
				except IndexError as e:
					company = ""
			else:
				position = ""
				company = ""

			try:
				image_url = member.find_element(*GroupPageLocators.IMAGE_URL).get_attribute("src")
			except NoSuchElementException as e:
				logging.error("Could not find img url")
	
			data = {
			"user_id" : [user_id],
			"first_name": [first_name],
			"last_name" : [last_name],
			"company" : [company],
			"position" : [position],
			"profile_url" : [profile_url],
			"image_url" : [image_url]
			}
			
			df = pd.DataFrame(data)
			df.to_sql(name=db_table, con=con, if_exists='append', index=False)

			count+=1
			if count%100==0:
				logging.info("Extracted {0} members".format(count))
		
		con.close()
		self.driver.close()
		logging.info("Data extraction finished")
		
class GroupPageLocators(object):
	MEMBER = (By.XPATH, "//div[contains(@id, 'recently_joined')]")
	IMAGE_URL = (By.XPATH, ".//img")
	NAME_LASTNAME = (By.XPATH, ".//div[contains(@class, 'uiProfileBlockContent')]")
	WORK = (By.XPATH, ".//div/div[2]/div[3]")
	END = (By.XPATH, "//div[contains(@class, 'fbProfileBrowserList expandedList fbProfileBrowserNoMoreItems')]")
