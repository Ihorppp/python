import fb_login
import fb_group
import fb_photo
import sys
import logging
try:
	import MySQLdb
	import urllib
	from pyvirtualdisplay import Display
except ImportError as e:
	print ("A required python module is missing!\n%s") % (e)
	sys.exit()

requests_logger = logging.getLogger('requests')
requests_logger.setLevel(logging.ERROR)

pyvirtualdisplay_logger = logging.getLogger('pyvirtualdisplay')
pyvirtualdisplay_logger.setLevel(logging.ERROR)

easyprocess_logger = logging.getLogger('easyprocess')
easyprocess_logger.setLevel(logging.ERROR)

def main():

	logging.basicConfig(filename='./example.log', level=logging.DEBUG)

	EMAIL = "email@gmail.com"
	PASSWORD = "password"
	GROUP_ID = "177730479651877"

	FB_PROFILE_PIC_WIDTH = 160
	FB_PROFILE_PIC_HEIGHT = 160
	PICS_FOLDER = "profile_pics"

	DB_HOST = "localhost"
	DB_NAME = "test"
	DB_USERNAME = "root"
	DB_PASSWORD = "password"
	DB_TABLE = "users"


	#starting virtual display
	display = Display(visible=0, size=(800, 600))
	display.start()
	
	#login
	facebook = fb_login.LoginPage("https://www.facebook.com/")
	facebook.check_title("Facebook")
	facebook.login(EMAIL, PASSWORD)
	
	#opening group
	group = fb_group.GroupPage(facebook.driver, GROUP_ID)
	group.get_data(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME, DB_TABLE)
	
	#create folder
	fb_photo.create_folder(PICS_FOLDER)

	logging.info("Opening DB to fetch user id and image urls")

	#extracting user id and image url to download pics	
	db = MySQLdb.connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME)
	cursor = db.cursor()
	sql = "Select image_url from %s" % (DB_TABLE)
	sql2 = "Select user_id from %s" % (DB_TABLE)
	try:
		cursor.execute(sql)
		img_urls = cursor.fetchall()
		cursor.execute(sql2)
		ids = cursor.fetchall()
	except Exception as e:
		logging.error("DB exception \n {0}".format(e))
	finally:
		db.close()

	logging.info("Data extraction from DB successful")

	logging.info("Starting image download")

	count = 0
	for i in range(len(img_urls)):
		image_name = ids[i][0]#user_id
		fb_photo.get_image(img_urls[i][0], image_name, PICS_FOLDER)

		count+=1
		if count%100==0:
			logging.info("Downloaded {0} images out of {1}".format(count, len(img_urls)))

	###download images via profile page###
	# facebook = fb_login.LoginPage("https://www.facebook.com/")
	# facebook.check_title("Facebook")
	# facebook.login(EMAIL, PASSWORD)
	# profile = fb_profile.ProfilePage(facebook.driver)
	# logging.info("Fetching and downloading images")
	# for i in range(len(img_urls)):
	# 	photo_link = profile.get_photo_link("https://www.facebook.com/"+ids[i][0])
	#	fb_photo.get_image(img_urls[i][0], image_name, PICS_FOLDER)
	

	###download images via facebook api(750 maximum)###
	# for i in range(len(data["user_id"])):
	# 	p_link = fb_photo.get_photo_link(data["user_id"][i], FB_PROFILE_PIC_WIDTH, FB_PROFILE_PIC_HEIGHT)
	# 	image_name = data["name"][i] + "_" + data["lastname"][i]
	# 	if fb_photo.get_image(p_link, image_name, PICS_FOLDER):
	# 		data["image"].append(image_name)

	
	# stopping virtual display
	display.stop()

	logging.info("Finished")

if __name__ == "__main__":
	main()


