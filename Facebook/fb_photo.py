try:
	import requests
	import json
	import os
except ImportError as e:
	print ("A required python module is missing!\n%s") % (e)
	sys.exit()

FB_PROFILE_PIC_WIDTH = 160
FB_PROFILE_PIC_HEIGHT = 160
PICS_FOLDER = "profile_pics"

def get_photo_link(user_id, img_width, img_height):
	url = "https://graph.facebook.com/"+ user_id +"?fields=picture.width({0}).height({1})".format(FB_PROFILE_PIC_WIDTH, FB_PROFILE_PIC_HEIGHT)
	resp = requests.get(url)
	json_resp = json.loads(resp.text)
	try:
		return json_resp["picture"]["data"]["url"]
	except:
		return None

def create_folder(folder_name):
	if not os.path.exists(folder_name):
		os.makedirs(folder_name)

def get_image(url, img_name, folder_name):
	if url == None:
		return False
	img_data = requests.get(url).content
	_format = ".jpg"
	if ".png" in url:
		_format = ".png"
	with open('./' + folder_name + '/' + img_name +_format, 'wb') as handler:
			handler.write(img_data)
	return True