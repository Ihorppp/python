from domain import Domain
import json

class DnsMadeEasy(object):
	
	def get_domains(self, ACobj):
		request = ACobj.request()
		return [Domain(json.dumps(data)) for data in request['data']]

	def get_domain(self, ACobj, domain_id):
		return Domain(json.dumps(ACobj.request(domain_id)))

	def add_domain(self, ACobj, domain_name):
		return ACobj.request(request_type="POST", data={"name" : domain_name})

	def delete_domain(self, ACobj, domain_id):
		return ACobj.request(domain_id, request_type="DELETE")