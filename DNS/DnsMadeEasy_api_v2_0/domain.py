import json
from record import Record

class Domain(object):

	def __init__(self, data):
		self.__dict__ = json.loads(data)

	def get_records(self, ACobj, record_name=None, record_type=None):
		if record_name is not None: 
			url_postfix = "{0}/records?recordName={1}".format(self.id, record_name)
		elif record_type is not None:
			url_postfix = "{0}/records?type={1}".format(self.id, record_type)
		elif record_name is not None and record_type is not None:
			url_postfix = "{0}/records?recordName={1}&type={2}".format(self.id, record_name, record_type)
		else:
			url_postfix = str(self.id) + "/records"
		request = ACobj.request(url_postfix)
		return [Record(json.dumps(data)) for data in request['data']]

	def get_record(self, ACobj, record_id):
		url_postfix = "{0}/records/{1}".format(self.id, record_id)
		return Record(ACobj.request(url_postfix))

	def add_record(self, ACobj, name, value, type="A", gtdLocation="DEFAULT", ttl=1800):
		data = {"name": name, "type" : type, "value" : value, "gtdLocation" : gtdLocation, "ttl" : ttl}
		url_postfix ="{0}/records/".format(self.id)
		return ACobj.request(url_postfix, request_type="POST", data=data)

	def delete_record(self, ACobj, record_id):
		url_postfix = "{0}/records/{1}".format(self.id, record_id)
		return ACobj.request(url_postfix, request_type="DELETE")

	def update_record(self, ACobj, record_id, name=None, type=None, value=None, gtdLocation=None, ttl=None):
		record = self.get_record(ACobj, record_id)
		if name is None: name = record.name
		if type is None: type = record.type
		if value is None: value = record.value
		if gtdLocation is None: gtdLocation = record.gtdLocation
		if ttl is None: ttl = record.ttl

		data = {"name": name, "type" : type, "value" : value, "id" : record_id, "gtdLocation" : gtdLocation, "ttl" : ttl}
		url_postfix = "{0}/records/{1}".format(self.id, record_id)
		return ACobj.request(url_postfix, request_type="PUT", data=data)