import requests
import json
from time import strftime, gmtime
import hashlib
import hmac

class ApiCall(object):

	def __init__(self, apikey, secret, base_url):
		self.apikey = apikey
		self.secret = secret
		self.base_url = base_url

	def request(self, url_postfix="", request_type="GET", data=None):

		def get_date_time():
			return strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())

		def get_hash(date_time):
			return hmac.new(self.secret.encode(), date_time.encode(), hashlib.sha1).hexdigest()

		def get_headers():
			date_time = get_date_time()
			hash_string = get_hash(date_time)
			headers = {	"x-dnsme-apiKey": self.apikey, 
						"x-dnsme-requestDate" : date_time,
						"x-dnsme-hmac" : hash_string, 
						"Accept": "application/json",
						"Content-Type": "application/json"
			}
			return headers

		if request_type is "GET":
			req = requests.get(self.base_url + url_postfix, headers=get_headers())
		elif request_type is "PUT":
			req = requests.put(self.base_url + url_postfix, headers=get_headers(), data=json.dumps(data))
		elif request_type is "POST":
			req = requests.post(self.base_url + url_postfix, headers=get_headers(), data=json.dumps(data))
		elif request_type is "DELETE":
			req = requests.delete(self.base_url + url_postfix, headers=get_headers())

		if req.status_code > 201:
			if req.status_code == 404:
				raise Exception("%s API endpoint not found" % url_postfix)
			else:
				raise Exception(req.text)

		if request_type is "GET":
			return json.loads(req.text)

		return req.status_code