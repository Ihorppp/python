from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage(object):

	def __init__(self):
		self.driver = webdriver.Firefox()


class LoginPage(BasePage):
	
	def __init__(self, url):
		BasePage.__init__(self)
		self.driver.get(url)

	def login(self, username, password, *args):
		usrnm_field = self.driver.find_element(*args[0])
		pwd_field = self.driver.find_element(*args[1])

		usrnm_field.send_keys(username)
		pwd_field.send_keys(password)
		login_button = self.driver.find_element(*args[2])
		login_button.click()
		self.driver.close()


class DnsPage(object):
	
	def __init__(self, url):
		BasePage.__init__(self)
		self.driver.get(url)


	def add_dns(self, server_name, ip_address, *args):
		add_button = self.driver.find_element(*args[0])
		add_button.click()

		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(*args[1]))

		name = self.driver.find_element(*args[1])
		ip_ad = self.driver.find_element(*args[2])
		name.send_keys(server_name)
		ip_ad.send_keys(ip_address)

		submit = self.driver.find_element(*args[3])
		submit.click()

		close = self.driver.find_element(*args[4])
		close.click()



class LoginPageLocator(object):
	USERNAME = (By.ID, "username")
	PASSWORD = (By.ID, "password")
	LOGIN_BUTTON = (By.XPATH, "//input[@value='Login']")



class DnsPageLocator(object):
	ADD_BUTTON = (By.ID, "add_gridA")
	INPUT_NAME = (By.ID, "name")
	IP_ADDRESS = (By.ID, "value")
	SUBMIT_BUTTON = (By.ID, "sData")
	CLOSE_BUTTON = (By.ID, "cData")

login = LoginPage("https://cp.dnsmadeeasy.com/")

login.login("username", "password", LoginPageLocator.USERNAME, LoginPageLocator.PASSWORD, LoginPageLocator.LOGIN_BUTTON)

dns_add = DnsPage("url_with_dns_addresses")

dns_add.add_dns("name","1.2.3.4", DnsPageLocator.ADD_BUTTON, DnsPageLocator.INPUT_NAME, DnsPageLocator.IP_ADDRESS, DnsPageLocator.SUBMIT_BUTTON, DnsPageLocator.CLOSE_BUTTON)
