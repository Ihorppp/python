from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# desired_capabilities = DesiredCapabilities.PHANTOMJS.copy()
# desired_capabilities['phantomjs.page.customHeaders.User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) ' \
#                                                                   'AppleWebKit/537.36 (KHTML, like Gecko) ' \
#                                                                   'Chrome/39.0.2171.95 Safari/537.36'
# driver = webdriver.PhantomJS(desired_capabilities=desired_capabilities)


driver = None
driver = webdriver.Firefox()

URL = "https://www.instagram.com/"
SEARCH_URL = "https://www.instagram.com/explore/tags/"
USERNAME = ""
PASSWORD = ""



def log_in(usrnm, pswd):

	driver.get(URL)

	try:
		WebDriverWait(driver, 10).until(
			EC.title_contains("Instagram")
			)
	except Exception as e:
		print "Error when loading url"
		return

	try:
		WebDriverWait(driver, 10).until(
			EC.presence_of_element_located((By.LINK_TEXT, "Log in"))
			)
		driver.find_element_by_link_text("Log in").click()
	except Exception as e:
		print "'Log in' link not found"
		return

	try:
		WebDriverWait(driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//button[contains(text(), 'Log in')]"))
			)
		username = driver.find_element_by_xpath("//input[@name='username']")
		username.send_keys(usrnm)
	except Exception as e:
		print "Username input field not found"
		return

	try:
		password = driver.find_element_by_xpath("//input[@name='password']")
		password.send_keys(pswd)
	except Exception as e:
		print "Password input field not found"

	try:
		driver.find_element_by_xpath("//button[contains(text(), 'Log in')]").click()
	except Exception as e:
		print "'Log in' button not found"

	try:
		WebDriverWait(driver, 10).until(
			EC.presence_of_element_located((By.LINK_TEXT, usrnm))
			)
	except Exception as e:
		print "Hm, something is wrong"


#searches for a given keyword
def search(keyword):
	driver.get(SEARCH_URL+keyword+"/")
	try:
		WebDriverWait(driver, 10).until(
			EC.title_contains('#'+keyword)
			)
	except Exception as e:
		print "Error when loading url"

	links = get_links_available(driver)
	return links

#gets all links from a page and returns ones containing /p/ tag, which are links to photos
def get_links_available(drvr):
	links = drvr.find_elements_by_xpath("//a[@href]")
	temp = [i.get_attribute("href") for i in links if "/p/" in i.get_attribute("href")]
	return temp

#returns user profile link from a photo
def get_profile_link(link):
	driver.get(link)
	profile_link = driver.find_element_by_xpath("//a[@href]").get_attribute("href")#first link is the profile link
	return profile_link

#returns dict containing number of followers and following of the user
def get_n_following_and_n_followers(drvr):
	n = {}
	parsed_text = get_parsed_text(drvr)

	for index in range(len(parsed_text)):
		if parsed_text[index]=="followers":
			n["followers"] = prettify(parsed_text[index-1])
		elif parsed_text[index]=="following":			
			n["following"] = prettify(parsed_text[index-1])
	return n

#extreme part of code to change 28,9 to 28.9 or 28.2k to 28200.0
def prettify(item):
	if "k" in str(item):
		temp = str(item).replace("k", "")
		return float(temp) * 1000
	if "," in str(item):
		temp = str(item).replace(",", ".")
		return float(temp)*1000
	return float(item)

#get user followers
def get_followers():
	pass

#follows user
def follow(link):
	driver.get(link)
	n_followers_following = get_n_following_and_n_followers(driver)
	#some business logic in here(filters etc.)
	print n_followers_following.get("followers")
	print n_followers_following.get("following")
	if (n_followers_following.get("followers") - n_followers_following.get("following")) > 150:
		return
	try:
		follow = driver.find_element_by_xpath("//button[contains(text(), 'Follow')]").click()
	except Exception as e:
		print "'Follow' button not found"
		return

#returns text from the page
def get_parsed_text(drvr):
	span = drvr.find_element_by_tag_name("span")
	span_splitted = []
	for i in span.text.encode("utf-8"):
		span_splitted.append(i)
	temp = ""
	parsed_text = []
	for letter in range(len(span_splitted)):
		if span_splitted[letter]!=' ' and span_splitted[letter]!='\n':
			temp+=span_splitted[letter]
		else:
			parsed_text.append(temp)
			temp=""
	return parsed_text



log_in(USERNAME, PASSWORD)

links = search("seaside")

for link in links:
	profile_link = get_profile_link(link)
	follow(profile_link)
