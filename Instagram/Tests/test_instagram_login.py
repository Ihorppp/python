import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys

URL = "https://www.instagram.com/"
USERNAME = ""
PASSWORD = ""

desired_capabilities = DesiredCapabilities.PHANTOMJS.copy()
desired_capabilities['phantomjs.page.customHeaders.User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) ' \
                                                                  'AppleWebKit/537.36 (KHTML, like Gecko) ' \
                                                                  'Chrome/39.0.2171.95 Safari/537.36'
driver = webdriver.PhantomJS(desired_capabilities=desired_capabilities)

# driver = None
# driver = webdriver.Firefox()

driver.get(URL)


class BasePage(object):

	def __init__(self, driver):
		self.driver = driver


class Login(BasePage):

	def title_matches(self, title):
		return title in self.driver.title

	def click_login_link(self):
		try:
			WebDriverWait(self.driver, 5).until(
				EC.presence_of_element_located(LoginPageLocator.LOGIN_LINK)
				)
			self.driver.find_element(*LoginPageLocator.LOGIN_LINK).click()
		except Exception as e:
			print "'Log in' link not found"

	def enter_creds(self, username, password):
		try:
			WebDriverWait(self.driver, 5).until(
				EC.presence_of_element_located(LoginPageLocator.USERNAME)
				)
			u = self.driver.find_element(*LoginPageLocator.USERNAME)
			u.send_keys(username)
		except Exception as e:
			print "Username field not found"

		try:
			WebDriverWait(self.driver, 5).until(
				EC.presence_of_element_located(LoginPageLocator.PASSWORD)
				)
			p = self.driver.find_element(*LoginPageLocator.PASSWORD)
			p.send_keys(password)
		except Exception as e:
			print "Password field not found"
		

	def click_login_button(self):
		try:
			WebDriverWait(self.driver, 5).until(
				EC.presence_of_element_located(LoginPageLocator.LOGIN_BUTTON)
				)
			self.driver.find_element(LoginPageLocator.LOGIN_BUTTON).click()
		except Exception as e:
			print "'Log in' button not found"

	def confirm_login(self, username):
		return (username in self.driver.page_source and "Profile" in self.driver.page_source)


class LoginPageLocator(object):
	LOGIN_LINK = (By.LINK_TEXT, "Log in")

	USERNAME = (By.XPATH, "//input[@name='usernames']")
	PASSWORD = (By.XPATH, "//input[@name='password']")

	LOGIN_BUTTON = (By.XPATH, "//button[contains(text(), 'Log in')]")


def test_login():

	login_page = Login(driver)
	assert login_page.title_matches("Instagram"), "Opening url failed"
	login_page.click_login_link()
	login_page.enter_creds(USERNAME, PASSWORD)
	login_page.click_login_button()
	assert login_page.confirm_login(USERNAME), "Login failed"

test_login()

