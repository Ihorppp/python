import sys
import subprocess
try:
	import arpreq#the same as running arp -n <ip_address>
except ImportError, e:
	print ("A required python module is missing!\n%s") % (e)
	sys.exit()

class Host(object):

	def __init__(self, ip_address, mac_address="", hostname=""):
		self.ip_address = ip_address
		self.mac_address = mac_address
		self.hostname = hostname

	def parse_output(self, output):
		parsed_output = []; 
		temp = ''
		for i in output:
			if i!=' ' and i!='\n' and i!='\t': 
				temp+=i
			elif i=='\n':
				if temp!='': parsed_output.append(temp) 
				parsed_output.append(i)
				temp=''
			elif temp!='': 
				parsed_output.append(temp) 
				temp=''
		return parsed_output

	def get_mac_address(self):
		arp = arpreq.arpreq(self.ip_address)
		if arp is not None:
			self.mac_address = arp
			return True
		return False

	#avahi
	def avahi(self):
		try:
			avahi = self.parse_output(subprocess.check_output(["avahi-resolve", "-a", self.ip_address], stderr=subprocess.STDOUT))
		except OSError:
			print ("avahi-resolve not found. Run apt-get install avahi-utils")
			sys.exit()
		#avahi failed
		if "Failed" in avahi:
			return False
		#avahi resolved hostname
		self.hostname = avahi[1]
		return True

	#netbios
	def nmb(self):
		try:
			nmb = self.parse_output(subprocess.check_output(["nmblookup", "-A", self.ip_address], stderr=subprocess.STDOUT))
		except OSError:
			print ("nmblookup not found. Run apt install samba")
			sys.exit()
		#raises CalledProcessError exception in case of timeout
		except Exception as e:
			return False
		#nmb resolved hostname
		self.hostname = nmb[6]
		return True
		

def main():
	
	host = Host(sys.argv[1])

	# Running "arp -n" does not show all active hosts in the network.
	# So as a result running "arp -n <ip_address>" also does not resolve mac address. Scanning subnet with nmap and then running arp makes no difference. 
	# But here is a strange thing -> running nmblookup on a host somehow makes it visibile in the network.

	if not host.nmb():
		if not host.avahi():
			print("Failed to fetch host name\n")

	if not host.get_mac_address():
		print("Failed to fetch Mac Address")

	print ("\nIP ADDRESS\n{0}\nMAC ADDRESS\n{1}\nHOST NAME\n{2}").format(host.ip_address, host.mac_address, host.hostname)

if __name__ == "__main__":
	main()
